#include "ns3/assert.h"
#include "ns3/address-utils.h"
#include "ns3/log.h"

#include "icmpv6-sixlowpan-header.h"


NS_LOG_COMPONENT_DEFINE ("Icmpv6SixLowpanHeader");

namespace ns3
{

#pragma region Icmpv6SixLowpanHeader-ClassImplementation

    NS_OBJECT_ENSURE_REGISTERED (Icmpv6SixLowpanHeader);

    TypeId Icmpv6SixLowpanHeader::GetTypeId()
    {
        static TypeId tid = TypeId("ns3::Icmpv6SixLowpanHeader")
                .SetParent<Header>()
                .AddConstructor<Icmpv6SixLowpanHeader>();

        return tid;
    }

    TypeId Icmpv6SixLowpanHeader::GetInstanceTypeId () const
    {
      NS_LOG_FUNCTION (this);
      return GetTypeId ();
    }

    Icmpv6SixLowpanHeader::Icmpv6SixLowpanHeader()
    {
        m_calcChecksum = true;
        m_checksum = 0;
        m_type = 0;
        m_code = 0;
        NS_LOG_FUNCTION (this);
    }

    Icmpv6SixLowpanHeader::~Icmpv6SixLowpanHeader ()
    {
        NS_LOG_FUNCTION (this);
    }

    uint8_t Icmpv6SixLowpanHeader::GetType() const
    {
        NS_LOG_FUNCTION(this);
        return m_type;
    }

    void Icmpv6SixLowpanHeader:: SetType(uint8_t type)
    {
        NS_LOG_FUNCTION(this);
        m_type = type;
    }

    uint8_t Icmpv6SixLowpanHeader::GetCode () const
    {
        NS_LOG_FUNCTION(this);
        return m_code;
    }

    void Icmpv6SixLowpanHeader::SetCode (uint8_t code)
    {
        NS_LOG_FUNCTION(this);
        m_code = code;
    }

    uint16_t Icmpv6SixLowpanHeader::GetChecksum () const
    {
        NS_LOG_FUNCTION(this);
        return m_checksum;
    }

    void Icmpv6SixLowpanHeader::SetChecksum (uint16_t checksum)
    {
        NS_LOG_FUNCTION(this);
        m_checksum = checksum;
    }

    void Icmpv6SixLowpanHeader::Print (std::ostream& os) const
    {
        NS_LOG_FUNCTION(this);
        os << "( type = " << (uint32_t)m_type << " code = " << (uint32_t)m_code << " checksum = " << (uint32_t)m_checksum << ")";
    }

    uint32_t Icmpv6SixLowpanHeader::GetSerializedSize () const
    {
        NS_LOG_FUNCTION(this);
        return 4;
    }

    void Icmpv6SixLowpanHeader::Serialize (Buffer::Iterator start) const
    {
        NS_LOG_FUNCTION(this);
    }

    uint32_t Icmpv6SixLowpanHeader::Deserialize (Buffer::Iterator start)
    {
        NS_LOG_FUNCTION(this);
        return GetSerializedSize();
    }

    void Icmpv6SixLowpanHeader::CalculatePseudoHeaderChecksum (Ipv6Address src, Ipv6Address dst, uint16_t length, uint8_t protocol)
    {
        NS_LOG_FUNCTION (this << src << dst << length << static_cast<uint32_t> (protocol));

        Buffer buf = Buffer (40);
        uint8_t tmp[16];
        Buffer::Iterator it;

        buf.AddAtStart (40);
        it = buf.Begin ();

        src.Serialize (tmp);
        it.Write (tmp, 16); /* source IPv6 address */
        dst.Serialize (tmp);
        it.Write (tmp, 16); /* destination IPv6 address */
        it.WriteU16 (0); /* length */
        it.WriteU8 (length >> 8); /* length */
        it.WriteU8 (length & 0xff); /* length */
        it.WriteU16 (0); /* zero */
        it.WriteU8 (0); /* zero */
        it.WriteU8 (protocol); /* next header */

        it = buf.Begin ();
        m_checksum = ~(it.CalculateIpChecksum (40));
    }

#pragma endregion

#pragma region Icmpv6SixLowpanOptionHeader-ClassImplementation

    NS_OBJECT_ENSURE_REGISTERED (Icmpv6SixLowpanOptionHeader);

    TypeId Icmpv6SixLowpanOptionHeader::GetTypeId ()
    {
        static TypeId tid = TypeId ("ns3::Icmpv6SixLowpanOptionHeader")
        .SetParent<Header> ()
        .AddConstructor<Icmpv6SixLowpanOptionHeader> ()
        ;
        return tid;
    }

    TypeId Icmpv6SixLowpanOptionHeader:: GetInstanceTypeId () const
    {
        NS_LOG_FUNCTION(this);
        return GetTypeId();
    }

    Icmpv6SixLowpanOptionHeader::Icmpv6SixLowpanOptionHeader()
    {
        NS_LOG_FUNCTION(this);
        m_type = 0;
        m_len = 0;
    }

    Icmpv6SixLowpanOptionHeader:: ~Icmpv6SixLowpanOptionHeader()
    {
        NS_LOG_FUNCTION(this);
    }

    uint8_t Icmpv6SixLowpanOptionHeader:: GetType () const
    {
        NS_LOG_FUNCTION(this);
        return m_type;
    }

    void Icmpv6SixLowpanOptionHeader::SetType (uint8_t type)
    {
        NS_LOG_FUNCTION (this << static_cast<uint32_t> (type));
        m_type = type;
    }

    uint8_t Icmpv6SixLowpanOptionHeader::GetLength () const
    {
        NS_LOG_FUNCTION(this);
        return m_len;
    }

    void Icmpv6SixLowpanOptionHeader::SetLength (uint8_t len)
    {
        NS_LOG_FUNCTION (this << static_cast<uint8_t> (len));
        m_len = len;
    }

    void Icmpv6SixLowpanOptionHeader::Print (std::ostream& os) const
    {
        NS_LOG_FUNCTION(this);
        os << "( type = " << (uint32_t)GetType () << " length = " << (uint32_t)GetLength () << ")";
    }

    uint32_t Icmpv6SixLowpanOptionHeader::GetSerializedSize () const
    {
        NS_LOG_FUNCTION(this);
        return m_len * 8;
    }

    void Icmpv6SixLowpanOptionHeader::Serialize (Buffer::Iterator start) const
    {
        NS_LOG_FUNCTION(this);

    }

    uint32_t Icmpv6SixLowpanOptionHeader::Deserialize (Buffer::Iterator start)
    {
        NS_LOG_FUNCTION(this);
        return GetSerializedSize();
    }

#pragma endregion

#pragma region Icmpv6SixLowpanDAR-ClassImplementation

    NS_OBJECT_ENSURE_REGISTERED (Icmpv6SixLowpanDAR);

    Icmpv6SixLowpanDAR:: Icmpv6SixLowpanDAR()
    {
        NS_LOG_FUNCTION (this);
        SetType (ICMPV6_LOWPAN_ND_DAR);
        SetCode(0);
        SetResereved(0);
        SetStatus(0);
        SetRegtrationLifetime(0);
        SetEui64(0);
        m_checksum = 0;
    }

    Icmpv6SixLowpanDAR:: ~Icmpv6SixLowpanDAR()
    {
        NS_LOG_FUNCTION (this);
    }


    TypeId Icmpv6SixLowpanDAR:: GetTypeId ()
    {
        static TypeId tid = TypeId ("ns3::Icmpv6SixLowpanDAR")
            .SetParent<Icmpv6SixLowpanHeader> ()
            .AddConstructor<Icmpv6SixLowpanDAR> ()
        ;
        return tid;
    }

    TypeId Icmpv6SixLowpanDAR:: GetInstanceTypeId () const
    {
        NS_LOG_FUNCTION (this);
        return GetTypeId ();
    }


    void Icmpv6SixLowpanDAR:: Print (std::ostream& os) const
    {
        NS_LOG_FUNCTION (this << &os);
        os << "( type = " << (uint32_t)GetType () << " (NS) code = " << (uint32_t)GetCode () << " checksum = " << (uint32_t)GetChecksum ()  << ")";
    }


    uint32_t Icmpv6SixLowpanDAR:: GetSerializedSize () const
    {
        NS_LOG_FUNCTION (this);
        return 32;
    }

    void Icmpv6SixLowpanDAR:: Serialize (Buffer::Iterator start) const
    {
        NS_LOG_FUNCTION (this << &start);
        uint8_t buff_reg_address[16];
        uint16_t checksum = 0;
        Buffer::Iterator i = start;

        i.WriteU8 (GetType ());
        i.WriteU8 (GetCode ());
        i.WriteU16 (0);
        i.WriteU8(GetStatus());
        i.WriteU8 (m_reserved); //m_reserved
        i.WriteU16(GetRegistrationLifetime());
        i.WriteHtolsbU64(GetEui64());
        m_reg_address.Serialize(buff_reg_address);
        i.Write(buff_reg_address,16);

        if (m_calcChecksum)
        {
            i = start;
            checksum = i.CalculateIpChecksum(i.GetSize(), m_checksum);
            i = start;
            i.Next(2);
            i.WriteU16(checksum);
        }
    }

    uint32_t Icmpv6SixLowpanDAR:: Deserialize (Buffer::Iterator start)
    {
        NS_LOG_FUNCTION (this << &start);
        uint8_t buf[16];
        Buffer::Iterator i = start;
        SetType(i.ReadU8());
        SetCode(i.ReadU8());
        m_checksum = i.ReadU16();
        SetStatus(i.ReadU8());
        m_reserved = i.ReadU8();
        SetRegtrationLifetime(i.ReadU16());
        SetEui64(i.ReadU64());
        i.Read(buf, 16);
        m_reg_address.Set(buf);

        return GetSerializedSize();
    }

    void Icmpv6SixLowpanDAR:: SetResereved(uint8_t reserved)
    {
        NS_LOG_FUNCTION(this << reserved);
        m_reserved = reserved;
    }


    uint8_t Icmpv6SixLowpanDAR::GetReserved() const
    {
        NS_LOG_FUNCTION(this);
        return m_reserved;
    }

    void Icmpv6SixLowpanDAR:: SetStatus(uint8_t status)
    {
        NS_LOG_FUNCTION(this << status);
        m_status = status;
    }

    uint8_t Icmpv6SixLowpanDAR:: GetStatus() const
    {
        NS_LOG_FUNCTION(this);
        return m_status;
    }

    void Icmpv6SixLowpanDAR:: SetRegtrationLifetime(uint16_t reg_lifetime)
    {
        NS_LOG_FUNCTION(this << reg_lifetime);
        m_reg_lifetime = reg_lifetime;
    }

    uint16_t Icmpv6SixLowpanDAR:: GetRegistrationLifetime() const
    {
        NS_LOG_FUNCTION(this);
        return m_reg_lifetime;
    }

    void Icmpv6SixLowpanDAR:: SetEui64(uint64_t eui64)
    {
        NS_LOG_FUNCTION(this << eui64);
        m_eui64 =  eui64;
    }

    uint64_t Icmpv6SixLowpanDAR:: GetEui64() const
    {
        NS_LOG_FUNCTION(this);
        return m_eui64;
    }

    void Icmpv6SixLowpanDAR:: SetRegisteredAddress(Ipv6Address reg_address)
    {
        NS_LOG_FUNCTION(this << reg_address);
        m_reg_address = reg_address;
    }

    Ipv6Address Icmpv6SixLowpanDAR:: GetRegisteredAddress() const
    {
        NS_LOG_FUNCTION(this);
        return m_reg_address;
    }
#pragma endregion

#pragma region Icmpv6SixLowpanDAC-ClassImplementation

    NS_OBJECT_ENSURE_REGISTERED (Icmpv6SixLowpanDAC);

    Icmpv6SixLowpanDAC:: Icmpv6SixLowpanDAC()
    {
        NS_LOG_FUNCTION (this);
        SetType (ICMPV6_LOWPAN_ND_DAC);
        SetCode(0);
        SetResereved(0);
        SetStatus(0);
        SetRegtrationLifetime(0);
        SetEui64(0);
        m_checksum = 0;
    }

    Icmpv6SixLowpanDAC:: ~Icmpv6SixLowpanDAC()
    {
        NS_LOG_FUNCTION (this);
    }


    TypeId Icmpv6SixLowpanDAC:: GetTypeId ()
    {
        static TypeId tid = TypeId ("ns3::Icmpv6SixLowpanDAC")
            .SetParent<Icmpv6SixLowpanHeader> ()
            .AddConstructor<Icmpv6SixLowpanDAR> ()
        ;
        return tid;
    }

    TypeId Icmpv6SixLowpanDAC:: GetInstanceTypeId () const
    {
        NS_LOG_FUNCTION (this);
        return GetTypeId ();
    }


    void Icmpv6SixLowpanDAC:: Print (std::ostream& os) const
    {
        NS_LOG_FUNCTION (this << &os);
        os << "( type = " << (uint32_t)GetType () << " (NS) code = " << (uint32_t)GetCode () << " checksum = " << (uint32_t)GetChecksum ()  << ")";
    }


    uint32_t Icmpv6SixLowpanDAC:: GetSerializedSize () const
    {
        NS_LOG_FUNCTION (this);
        return 32;
    }

    void Icmpv6SixLowpanDAC:: Serialize (Buffer::Iterator start) const
    {
        NS_LOG_FUNCTION (this << &start);
        uint8_t buff_reg_address[16];
        uint16_t checksum = 0;
        Buffer::Iterator i = start;

        i.WriteU8 (GetType ());
        i.WriteU8 (GetCode ());
        i.WriteU16 (0);
        i.WriteU8(GetStatus());
        i.WriteU8 (m_reserved); //m_reserved
        i.WriteU16(GetRegistrationLifetime());
        i.WriteHtolsbU64(GetEui64());
        m_reg_address.Serialize(buff_reg_address);
        i.Write(buff_reg_address,16);

        if (m_calcChecksum)
        {
            i = start;
            checksum = i.CalculateIpChecksum(i.GetSize(), m_checksum);
            i = start;
            i.Next(2);
            i.WriteU16(checksum);
        }
    }

    uint32_t Icmpv6SixLowpanDAC:: Deserialize (Buffer::Iterator start)
    {
        NS_LOG_FUNCTION (this << &start);
        uint8_t buf[16];
        Buffer::Iterator i = start;
        SetType(i.ReadU8());
        SetCode(i.ReadU8());
        m_checksum = i.ReadU16();
        SetStatus(i.ReadU8());
        m_reserved = i.ReadU8();
        SetRegtrationLifetime(i.ReadU16());
        SetEui64(i.ReadU64());
        i.Read(buf, 16);
        m_reg_address.Set(buf);

        return GetSerializedSize();
    }

    void Icmpv6SixLowpanDAC:: SetResereved(uint8_t reserved)
    {
        NS_LOG_FUNCTION(this << reserved);
        m_reserved = reserved;
    }


    uint8_t Icmpv6SixLowpanDAC::GetReserved() const
    {
        NS_LOG_FUNCTION(this);
        return m_reserved;
    }

    void Icmpv6SixLowpanDAC:: SetStatus(uint8_t status)
    {
        NS_LOG_FUNCTION(this << status);
        m_status = status;
    }

    uint8_t Icmpv6SixLowpanDAC:: GetStatus() const
    {
        NS_LOG_FUNCTION(this);
        return m_status;
    }

    void Icmpv6SixLowpanDAC:: SetRegtrationLifetime(uint16_t reg_lifetime)
    {
        NS_LOG_FUNCTION(this << reg_lifetime);
        m_reg_lifetime = reg_lifetime;
    }

    uint16_t Icmpv6SixLowpanDAC:: GetRegistrationLifetime() const
    {
        NS_LOG_FUNCTION(this);
        return m_reg_lifetime;
    }

    void Icmpv6SixLowpanDAC:: SetEui64(uint64_t eui64)
    {
        NS_LOG_FUNCTION(this << eui64);
        m_eui64 =  eui64;
    }

    uint64_t Icmpv6SixLowpanDAC:: GetEui64() const
    {
        NS_LOG_FUNCTION(this);
        return m_eui64;
    }

    void Icmpv6SixLowpanDAC:: SetRegisteredAddress(Ipv6Address reg_address)
    {
        NS_LOG_FUNCTION(this << reg_address);
        m_reg_address = reg_address;
    }

    Ipv6Address Icmpv6SixLowpanDAC:: GetRegisteredAddress() const
    {
        NS_LOG_FUNCTION(this);
        return m_reg_address;
    }
#pragma endregion

#pragma region Icmpv6SixLowpamOPtionARO-ClassImplementation

    NS_OBJECT_ENSURE_REGISTERED (Icmpv6SixLowpanOptionARO);

    TypeId Icmpv6SixLowpanOptionARO:: GetInstanceTypeId () const
    {
        static TypeId tid = TypeId ("ns3::Icmpv6SixLowpanOptionARO")
            .SetParent<Icmpv6SixLowpanOptionHeader> ()
            .AddConstructor<Icmpv6SixLowpanOptionARO> ()
            ;
        return tid;
    }

    Icmpv6SixLowpanOptionARO:: Icmpv6SixLowpanOptionARO ()
    {
        NS_LOG_FUNCTION(this);
        SetType(Icmpv6SixLowpanHeader::OptionType_e::ICMPV6_LOWPAN_ND_ARO);
        SetLength(1);
        SetResereved(0);
    }

    Icmpv6SixLowpanOptionARO:: ~Icmpv6SixLowpanOptionARO ()
    {
        NS_LOG_FUNCTION(this);
    }

    void Icmpv6SixLowpanOptionARO:: Print (std::ostream& os) const
    {
        NS_LOG_FUNCTION(this << &os);
        os << "( type = " << (uint32_t)GetType () << " length = " << (uint32_t)GetLength () << " status = " << m_status << "reserved" << m_reserved << "registration_lifetime" << m_reg_lifetime <<"eui64" << m_eui64 <<  ")";
    }

    uint32_t Icmpv6SixLowpanOptionARO:: GetSerializedSize () const
    {
        NS_LOG_FUNCTION(this);
        return 16; /* m_len = 2 so the real size is multiple by 8 hence 16*/
    }

    void Icmpv6SixLowpanOptionARO:: Serialize (Buffer::Iterator start) const
    {
        NS_LOG_FUNCTION(this << &start);
        Buffer::Iterator i = start;
        i.WriteU8 (GetType ());
        i.WriteU8 (GetLength ());
        i.WriteU8 (GetStatus ());
        i.WriteU8 (GetReserved ());
        i.WriteHtonU16(GetRegistrationLifetime());
        i.WriteHtonU64(GetEui64());
    }

    uint32_t Icmpv6SixLowpanOptionARO:: Deserialize (Buffer::Iterator start)
    {
        NS_LOG_FUNCTION (this << &start);
        Buffer::Iterator i = start;
        SetType (i.ReadU8 ());
        SetLength(i.ReadU8());
        SetStatus(i.ReadU8());
        SetResereved(i.ReadU16());
        SetRegtrationLifetime(i.ReadU16());
        SetEui64(i.ReadU64());

        return GetSerializedSize();
    }

    void Icmpv6SixLowpanOptionARO:: SetStatus(uint8_t status)
    {
        NS_LOG_FUNCTION(this << status);
        m_status = status;
    }

    uint8_t Icmpv6SixLowpanOptionARO::GetStatus() const
    {
        NS_LOG_FUNCTION(this);
        return m_status;
    }

    void Icmpv6SixLowpanOptionARO:: SetResereved(uint8_t reserved)
    {
        NS_LOG_FUNCTION(this <<reserved);
        m_reserved = reserved;
    }

    uint8_t Icmpv6SixLowpanOptionARO:: GetReserved() const
    {
        NS_LOG_FUNCTION(this);
        return m_reserved;
    }

    void Icmpv6SixLowpanOptionARO:: SetRegtrationLifetime(uint16_t reg_lifetime)
    {
        NS_LOG_FUNCTION(this << reg_lifetime);
        m_reg_lifetime = reg_lifetime;
    }

    uint16_t Icmpv6SixLowpanOptionARO:: GetRegistrationLifetime() const
    {
        NS_LOG_FUNCTION(this);
        return m_reg_lifetime;
    }

    void Icmpv6SixLowpanOptionARO:: SetEui64(uint64_t eui64)
    {
        NS_LOG_FUNCTION(this << eui64);
        m_eui64 = eui64;
    }

    uint64_t Icmpv6SixLowpanOptionARO:: GetEui64() const
    {
        NS_LOG_FUNCTION(this);
        return m_eui64;
    }
#pragma endregion

#pragma region Icmpv6SixLowpanOption6CO-ClassImplementatoin

    NS_OBJECT_ENSURE_REGISTERED (Icmpv6SixLowpanOption6CO);

    TypeId Icmpv6SixLowpanOption6CO:: GetTypeId ()
    {
        static TypeId tid = TypeId ("ns3::Icmpv6SixLowpanOption6CO")
            .SetParent<Icmpv6SixLowpanOptionHeader> ()
            .AddConstructor<Icmpv6SixLowpanOption6CO> ()
        ;
        return tid;
    }

    TypeId Icmpv6SixLowpanOption6CO:: GetInstanceTypeId (void) const
    {
        NS_LOG_FUNCTION(this);
        return GetTypeId();
    }

    Icmpv6SixLowpanOption6CO:: Icmpv6SixLowpanOption6CO ()
    {
        NS_LOG_FUNCTION(this);
        SetType(Icmpv6SixLowpanHeader::OptionType_e::ICMPV6_LOWPAN_ND_6CO);
        SetLength(2);/*Lenght is 2 or 3 depending on context prefix*/
        SetResereved (0);
    }

    Icmpv6SixLowpanOption6CO:: ~Icmpv6SixLowpanOption6CO ()
    {
        NS_LOG_FUNCTION(this);
    }

    void Icmpv6SixLowpanOption6CO:: Print (std::ostream& os) const
    {

    }

    uint32_t Icmpv6SixLowpanOption6CO:: GetSerializedSize () const
    {
        NS_LOG_FUNCTION(this);
        //TODO
        return 3;
    }

    void Icmpv6SixLowpanOption6CO:: Serialize (Buffer::Iterator start) const
    {
        NS_LOG_FUNCTION(this);
    }

    uint32_t Icmpv6SixLowpanOption6CO:: Deserialize (Buffer::Iterator start)
    {
        NS_LOG_FUNCTION(this);
        //TODO
        return 0;
    }

    void Icmpv6SixLowpanOption6CO:: SetResereved(uint16_t reserved)
    {
        NS_LOG_FUNCTION(this);
        m_reserved = reserved;
    }

    uint8_t Icmpv6SixLowpanOption6CO:: GetReserved() const
    {
        NS_LOG_FUNCTION(this);
        return m_reserved;
    }

    void Icmpv6SixLowpanOption6CO:: SetStatus(uint8_t status)
    {
        NS_LOG_FUNCTION(this);
        m_status = status;
    }

    uint8_t Icmpv6SixLowpanOption6CO:: GetStatus() const
    {
        NS_LOG_FUNCTION(this);
        return m_status;
    }

    void Icmpv6SixLowpanOption6CO:: SetContextLen(uint8_t context_len)
    {
        NS_LOG_FUNCTION(this);
        m_context_len = context_len;
    }

    uint8_t Icmpv6SixLowpanOption6CO:: GetContextLen() const
    {
        NS_LOG_FUNCTION(this);
        return m_context_len;
    }

    void Icmpv6SixLowpanOption6CO:: SetCFlag(bool c_flag)
    {
        NS_LOG_FUNCTION(this);
        m_c_flag = c_flag;
    }

    bool Icmpv6SixLowpanOption6CO:: GetCFlag() const
    {
        NS_LOG_FUNCTION(this);
        return m_c_flag;
    }

    void Icmpv6SixLowpanOption6CO:: SetContextId(uint8_t cid)
    {
        NS_LOG_FUNCTION(this);
        m_cid = cid;
    }

    uint8_t Icmpv6SixLowpanOption6CO:: GetContextId() const
    {
        NS_LOG_FUNCTION(this);
        return m_cid;
    }

    void Icmpv6SixLowpanOption6CO:: SetRes(uint8_t res)
    {
        NS_LOG_FUNCTION(this);
        m_res = res;
    }

    uint8_t Icmpv6SixLowpanOption6CO:: GetRes() const
    {
        NS_LOG_FUNCTION(this);
        return m_res;
    }

    void Icmpv6SixLowpanOption6CO:: SetValidLifetime(uint16_t valid_lifetime)
    {
        NS_LOG_FUNCTION(this);
        m_valid_lifetime = valid_lifetime;
    }

    uint8_t Icmpv6SixLowpanOption6CO:: GetValidLifetime() const
    {
        NS_LOG_FUNCTION(this);
        return m_valid_lifetime;
    }

    void Icmpv6SixLowpanOption6CO:: SetContextPrefix(std::vector<uint8_t> context_prefix)
    {
        NS_LOG_FUNCTION(this);
        m_context_prefix = context_prefix;
		SetContextLen(context_prefix.size);
    }

    std::vector<uint8_t> Icmpv6SixLowpanOption6CO:: GetContextPrefix() const
    {
        NS_LOG_FUNCTION(this);
        return m_context_prefix;
    }

#pragma endregion

#pragma region Icmpv6SixLowpanOptionABRO-ClassImplementatoin

    Icmpv6SixLowpanOptionABRO:: Icmpv6SixLowpanOptionABRO()
    {
        NS_LOG_FUNCTION(this);
		SetType(Icmpv6SixLowpanHeader::OptionType_e::ICMPV6_LOWPAN_ND_ABRO);
        SetLength(3);/*\ref rfc 6775 page 19*/
        SetVersionHigh(0);
        SetVersionLow(0);
        SetValidLifetime(0);
    }

    TypeId Icmpv6SixLowpanOptionABRO:: GetTypeId ()
    {
        static TypeId tid = TypeId ("ns3::Icmpv6SixLowpanOptionABRO")
            .SetParent<Icmpv6SixLowpanOptionHeader> ()
            .AddConstructor<Icmpv6SixLowpanOptionABRO> ()
        ;
        return tid;
    }

    TypeId Icmpv6SixLowpanOptionABRO:: GetInstanceTypeId (void) const
    {
        NS_LOG_FUNCTION(this);
        return GetTypeId ();
    }

    Icmpv6SixLowpanOptionABRO:: ~Icmpv6SixLowpanOptionABRO()
    {
        NS_LOG_FUNCTION(this);
    }

    void Icmpv6SixLowpanOptionABRO:: Print (std::ostream& os) const
    {
        NS_LOG_FUNCTION(this << "To do, incomplete implementation");	
    }

    uint32_t Icmpv6SixLowpanOptionABRO:: GetSerializedSize () const
    {
        NS_LOG_FUNCTION(this);
        return 3*8; /*m_len = 3 actual length will be 3*8 */
    }

    void Icmpv6SixLowpanOptionABRO:: Serialize (Buffer::Iterator start) const
    {
        NS_LOG_FUNCTION(this);
    }

    uint32_t Icmpv6SixLowpanOptionABRO:: Deserialize (Buffer::Iterator start)
    {
        NS_LOG_FUNCTION(this);
    }
	   
    void Icmpv6SixLowpanOptionABRO::SetVersionLow(uint16_t version_low)
    {
        NS_LOG_FUNCTION(this);
    }

    uint16_t Icmpv6SixLowpanOptionABRO:: GetVersionLow() const
    {
        NS_LOG_FUNCTION(this);
    }

    void Icmpv6SixLowpanOptionABRO:: SetVersionHigh(uint16_t version_high)
    {
        NS_LOG_FUNCTION(this << version_high);
    }

    uint16_t Icmpv6SixLowpanOptionABRO:: GetVersionHigh() const
    {
        NS_LOG_FUNCTION(this);
    }

    void Icmpv6SixLowpanOptionABRO:: SetValidLifetime(uint16_t valid_lifetime)
    {
        NS_LOG_FUNCTION(this << valid_lifetime);
    }

    uint16_t Icmpv6SixLowpanOptionABRO::GetValidLifetime() const
    {
        NS_LOG_FUNCTION(this);
    }

    void Icmpv6SixLowpanOptionABRO:: Set6BlrAddress(Ipv6Address sixblr_address)
    {
        NS_LOG_FUNCTION(this << sixblr_address);
    }

    Ipv6Address Icmpv6SixLowpanOptionABRO:: Get6BlrAddress() const
    {
        NS_LOG_FUNCTION(this);
    }


#pragma endregion

} //namespace ns3