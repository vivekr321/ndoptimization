#ifndef ICMPV6_SIXLOWPAN_PROTOCOL_H
#define ICMPV6_SIXLOWPAN_PROTOCOL_H

#include <list>

#include "ns3/ipv6-address.h"
#include "ns3/ipv6-interface.h"
#include "ns3/random-variable-stream.h"
#include "ns3/icmpv6-header.h"
#include "ns3/ip-l4-protocol.h"

#include "icmpv6-sixlowpan-header.h"


namespace ns3{

    class NetDevice;
    class Node;
    class Packet;
    class TraceContext;
    class NdiscCache;

    /*
    * \class Icmpv6SixLowPanProtocol
    * \brief An implementation of ICMPV6 ND Optimizations
    */
    class Icmpv6SixLowPanProtocol: public IpL4Protocol {
    public:
        /**
        * \brief Interface ID
        */
        static TypeId GetTypeId ();

        /**
        * \brief ICMPv6 protocol number (58).
        */
        static const uint8_t PROT_NUMBER;

        /**
        * \brief Constructor.
        */
        Icmpv6SixLowPanProtocol ();

        /**
        * \brief Destructor.
        */
        virtual ~Icmpv6SixLowPanProtocol ();

#pragma region SixLBR-Constants

        /**
        * \brief Minimum context change delay (new)
        */
        static const uint8_t MIN_CONTEXT_CHANGE_DELAY;

#pragma endregion

#pragma region SixLR-Constants

        /**
        * \brief Maximum retransmission advertisements
        */
        static const uint8_t MAX_RTR_ADVERTISEMENTS;

        /**
        * \brief Minimum delay between RA (modified)
        */
        static const uint8_t MIN_DELAY_BETWEEN_RAS;


        /**
        * \brief Maximum RA delay time (modified)
        */
        static const uint8_t MAX_RA_DELAY_TIME;


        /**
        * \brief Tentative NCE lifetime (new)
        */
        static const uint8_t TENTATIVE_NCE_LIFETIME;

#pragma endregion

#pragma region Router Constants

        /**
        * \brief Multihop Hop limit (new)
        */
        static const uint8_t MULTIHOP_HOPLIMIT;

#pragma endregion

#pragma region Host Constants

        /**
        * \brief Neighbor Discovery host constants : max RS transmission.
        */
        static const uint8_t MAX_RTR_SOLICITATIONS;

        /**
        * \brief Neighbor Discovery host constants : RS interval.
        */
        static const uint8_t RTR_SOLICITATION_INTERVAL;

        /**
        * \brief Neighbor Discovery host constants : max RTR solicitations interval.
        */
        static const uint8_t MAX_RTR_SOLICITATION_INTERVAL;

#pragma endregion

#pragma region Forge packets

        /**
        * \brief Forge a Router Solicitation.
        * \param src source IPv6 address
        * \param dst destination IPv6 address
        * \param hardwareAddress our mac address
        * \return RS packet (with IPv6 header)
        */
        Ptr<Packet> ForgeRS (Ipv6Address src, Ipv6Address dst, Address hardwareAddress);

        /**
        * \brief Forge a Neighbor Solicitation.
        * \param src source IPv6 address
        * \param dst destination IPv6 address
        * \param target target IPv6 address
        * \param hardwareAddress our mac address
        * \return NS packet (with IPv6 header)
        */
        Ptr<Packet> ForgeNS (Ipv6Address src, Ipv6Address dst, Ipv6Address target, Address hardwareAddress);

        /**
        * \brief Forge a Neighbor Advertisement.
        * \param src source IPv6 address
        * \param dst destination IPv6 address
        * \param hardwareAddress our mac address
        * \param flags flags (bitfield => R (4), S (2), O (1))
        * \return NA packet (with IPv6 header)
        */
        Ptr<Packet> ForgeNA (Ipv6Address src, Ipv6Address dst, Address* hardwareAddress, uint8_t flags);

#pragma endregion

#pragma region send packets

        /**
        * \brief Send a Neighbor Adverstisement.
        * \param src source IPv6 address
        * \param dst destination IPv6 address
        * \param hardwareAddress our MAC address
        * \param flags to set (4 = flag R, 2 = flag S, 3 = flag O)
        */
        void SendNA (Ipv6Address src, Ipv6Address dst, Address* hardwareAddress, uint8_t flags);

        /**
        * \brief Send a Neighbor Solicitation.
        * \param src source IPv6 address
        * \param dst destination IPv6 address
        * \param target target IPv6 address
        * \param hardwareAddress our mac address
        */
        void SendNS (Ipv6Address src, Ipv6Address dst, Ipv6Address target, Address hardwareAddress);


#pragma endregion

        /**
        * \brief Receive method.
        * \param p the packet
        * \param src source address
        * \param dst destination address
        * \param interface the interface from which the packet is coming
        */
        virtual enum IpL4Protocol::RxStatus Receive (Ptr<Packet> p,
                                            Ipv6Header const &header,
                                            Ptr<Ipv6Interface> interface);

protected:
        /**
        * \brief Dispose this object.
        */
        virtual void DoDispose ();

private:
        typedef std::list<Ptr<NdiscCache> > CacheList;

        /**
        * \brief The node.
        */
        Ptr<Node> m_node;

        /**
        * \brief A list of cache by device.
        */
        CacheList m_cacheList;

        /**
        * \brief Random jitter before sending solicitations
        */
        Ptr<RandomVariableStream> m_solicitationJitter;

        /**
        * \brief Receive Neighbor Solicitation method.
        * \param p the packet
        * \param src source address
        * \param dst destination address
        * \param interface the interface from which the packet is coming
        */
        void HandleNS (Ptr<Packet> p, Ipv6Address const &src, Ipv6Address const &dst, Ptr<Ipv6Interface> interface);

        /**
        * \brief Receive Router Solicitation method.
        * \param p the packet
        * \param src source address
        * \param dst destination address
        * \param interface the interface from which the packet is coming
        */
        void HandleRS (Ptr<Packet> p, Ipv6Address const &src, Ipv6Address const &dst, Ptr<Ipv6Interface> interface);

        /**
        * \brief Receive Router Advertisement method.
        * \param p the packet
        * \param src source address
        * \param dst destination address
        * \param interface the interface from which the packet is coming
        */
        void HandleRA (Ptr<Packet> p, Ipv6Address const &src, Ipv6Address const &dst, Ptr<Ipv6Interface> interface);

        /**
        * \brief Receive Neighbor Advertisement method.
        * \param p the packet
        * \param src source address
        * \param dst destination address
        * \param interface the interface from which the packet is coming
        */
        void HandleNA (Ptr<Packet> p, Ipv6Address const &src, Ipv6Address const &dst, Ptr<Ipv6Interface> interface);

        /**
        * \brief Link layer address option processing (SLLAO )
        * \param lla LLA option
        * \param src source address
        * \param dst destination address
        * \param interface the interface from which the packet is coming
        */
        void ReceiveLLA (Icmpv6OptionLinkLayerAddress lla, Ipv6Address const &src, Ipv6Address const &dst, Ptr<Ipv6Interface> interface);



    } //Icmpv6SixLowPanProtocol

} // NS3



#endif //ICMPV6_SIXLOWPAN_PROTOCOL_H