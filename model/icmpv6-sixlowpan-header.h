#ifndef ICMPV6_SIXLOWPAN_HEADER_H
#define ICMPV6_SIXLOWPAN_HEADER_H

#include "ns3/header.h"
#include "ns3/ipv6-address.h"
#include "ns3/packet.h"


/*refer RFC6775*/
namespace ns3 {

/**
* \brief Generic Message header for ICMPV6 over 6 LOWPAN
*/
class Icmpv6SixLowpanHeader : public Header
{
public:
    /**
    * \enum Type_e
    * \brief ICMPv6 6lowpan type code.
    */
    enum Type_e{
        ICMPV6_LOWPAN_ND_DAR = 157, /*Duplicate Address request*/
        ICMPV6_LOWPAN_ND_DAC = 158  /*Duplicate address confirmation*/
    };

    /**
    * \enum OptionType_e
    * \brief ICMPv6 6lowpan Option type code.
    */
    enum OptionType_e{
        ICMPV6_LOWPAN_ND_ARO = 33,      /*Address Registraion option*/
        ICMPV6_LOWPAN_ND_6CO = 34,      /*6lowpan context option*/
        ICMPV6_LOWPAN_ND_ABRO = 35,     /*Authoritative Border Router Option*/
    };

    /**
    * \brief Get the UID of this class.
    * \return UID
    */
    static TypeId GetTypeId ();

    /**
    * \brief Get the instance type ID.
    * \return instance type ID
    */
    virtual TypeId GetInstanceTypeId () const;

    /**
    * \brief Constructor.
    */
    Icmpv6SixLowpanHeader();

    virtual ~Icmpv6SixLowpanHeader();

    /**
    * \brief Get the type field.
    * \return type of ICMPv6 6lowpan message
    */
    uint8_t GetType () const;

    /**
    * \brief Set the type.
    * \param type type to set
    */
    void SetType (uint8_t type);

    /**
    * \brief Get the code field.
    * \return code of ICMPv6 message
    */
    uint8_t GetCode () const;
    /**
    * \brief Set the code field.
    * \param code code to set
    */
    void SetCode (uint8_t code);

    /**
    * \brief Get the checksum.
    * \return checksum
    */
    uint16_t GetChecksum () const;

    /**
    * \brief Set the checksum.
    * \param checksum to set
    */
    void SetChecksum (uint16_t checksum);

    /**
    * \brief Print informations.
    * \param os output stream
    */
    virtual void Print (std::ostream& os) const;

    /**
    * \brief Get the serialized size.
    * \return serialized size
    */
    virtual uint32_t GetSerializedSize () const;

    /**
    * \brief Serialize the packet.
    * \param start start offset
    */
    virtual void Serialize (Buffer::Iterator start) const;

    /**
    * \brief Deserialize the packet.
    * \param start start offset
    * \return length of packet
    */
    virtual uint32_t Deserialize (Buffer::Iterator start);

    /**
    * \brief Calculate pseudo header checksum for IPv6.
    * \param src source address
    * \param dst destination address
    * \param length length
    * \param protocol the protocol number to use in the
    * underlying IPv6 packet.
    */
    void CalculatePseudoHeaderChecksum (Ipv6Address src, Ipv6Address dst, uint16_t length, uint8_t protocol);
protected:
    /**
    * \brief Checksum enable or not.
    */
    bool m_calcChecksum;

    /**
    * \brief The checksum.
    */
    uint16_t m_checksum;

private:
    /**
    * \brief The type.
    */
    uint8_t m_type;

    /**
    * \brief The code.
    */
    uint8_t m_code;
};

/**
* \brief Generic Option header for ICMPV6 over 6 LOWPAN
*/
class Icmpv6SixLowpanOptionHeader : public Header
{
public:
    /**
    * \brief Get the UID of this class.
    * \return UID
    */
    static TypeId GetTypeId ();

    /**
    * \brief Get the instance type ID.
    * \return instance type ID
    */
    virtual TypeId GetInstanceTypeId () const;

    /**
    * \brief Constructor.
    */
    Icmpv6SixLowpanOptionHeader();

    /**
    * \brief Destructor.
    */
    virtual ~Icmpv6SixLowpanOptionHeader();

    /**
    * \brief Get the type of the option.
    * \return type
    */
    uint8_t GetType () const;

    /**
    * \brief Set the type of the option.
    * \param type the type to set
    */
    void SetType (uint8_t type);

    /**
    * \brief Get the length of the option in 8 bytes unit.
    * \return length of the option
    */
    uint8_t GetLength () const;

    /**
    * \brief Set the length of the option.
    * \param len length value to set
    */
    void SetLength (uint8_t len);

    /**
    * \brief Print informations.
    * \param os output stream
    */
    virtual void Print (std::ostream& os) const;

    /**
    * \brief Get the serialized size.
    * \return serialized size
    */
    virtual uint32_t GetSerializedSize () const;

    /**
    * \brief Serialize the packet.
    * \param start start offset
    */
    virtual void Serialize (Buffer::Iterator start) const;

    /**
    * \brief Deserialize the packet.
    * \param start start offset
    * \return length of packet
    */
    virtual uint32_t Deserialize (Buffer::Iterator start);

private:
    /**
    * \brief The type.
    */
    uint8_t m_type;

    /**
    * \brief The length.
    */
    uint8_t m_len;
};


/**
* \brief Message header for ICMPV6 over 6 LOWPAN DAR (Duplicate address Request)
*/
class Icmpv6SixLowpanDAR: public Icmpv6SixLowpanHeader
{
public:
    /**
    * \brief Constructor.
    */
    Icmpv6SixLowpanDAR();
    /**
    * \brief Destructor.
    */
    virtual ~Icmpv6SixLowpanDAR();

    /**
    * \brief Get the UID of this class.
    * \return UID
    */
    static TypeId GetTypeId ();

    /**
    * \brief Get the instance type ID.
    * \return instance type ID
    */
    virtual TypeId GetInstanceTypeId () const;

    /**
    * \brief Print informations.
    * \param os output stream
    */
    virtual void Print (std::ostream& os) const;

    /**
    * \brief Get the serialized size.
    * \return serialized size
    */
    virtual uint32_t GetSerializedSize () const;

    /**
    * \brief Serialize the packet.
    * \param start start offset
    */
    virtual void Serialize (Buffer::Iterator start) const;

    /**
    * \brief Deserialize the packet.
    * \param start start offset
    * \return length of packet
    */
    virtual uint32_t Deserialize (Buffer::Iterator start);

    /**
    * \brief Get the reserved field.
    * \return reserved value
    */
    void SetResereved(uint8_t reserved);

    /**
    * \brief Set the reserved field.
    * \param reserved the reserved value
    */
    uint8_t GetReserved() const;

    /**
    * \brief Set the status field.
    * \param status the status value
    */
    void SetStatus(uint8_t status);

    /**
    * \brief Get the status field.
    * \return status value
    */
    uint8_t GetStatus() const;

    /**
    * \brief Set the status field.
    * \param status the status value
    */
    void SetRegtrationLifetime(uint16_t reg_lifetime);

    /**
    * \brief Get Registration Lifetime
    * \return Registration lifetime value
    */
    uint16_t GetRegistrationLifetime() const;

    /**
    * \brief Get Eui64 value
    * \param eui64 the eui64 value
    */
    void SetEui64(uint64_t eui64);

    /**
    * \brief Get the Eui64 value
    * \return eui64 the eui64 value
    */
    uint64_t GetEui64() const;

    /**
    * \brief Set host address that was contained in IpV6 source field
    * \param reg_address the host address value
    */
    void SetRegisteredAddress(Ipv6Address reg_address);

    /**
    * \brief Get Registered address
    * \return Ipv6Address the host address
    */
    Ipv6Address GetRegisteredAddress() const;

private:

    /**
    *   \brief This field is unused. It MUST be initialized
    *           to zero by the sender and MUST be ignored by the receiver
    */
    uint8_t m_reserved;

    /**
    * \brief 8-bit unsigned integer. Indicates the status of a registration in the DAC.
             MUST be set to 0 in the DAR
    */
    uint8_t m_status;

    /**
    * \brief 16-bit unsigned integer. The amount of time
    *        in units of 60 seconds that the 6LBR should
    *        retain the DAD table entry for the Registered
    *        Address. A value of 0 indicates in a DAR that
    *        the DAD table entry should be removed.
    */
    uint16_t m_reg_lifetime;

    /**
    * \brief 64 bits. This field is used to uniquely
    *        identify the interface of the Registered
    *        Address by including the EUI-64 identifier
    *        [EUI64] assigned to it unmodified
    */
    uint64_t m_eui64;

    /**
    * \brief 128-bit field. Carries the host address that
    *       was contained in the IPv6 Source field in the
    *       NS that contained the ARO sent by the host.
    */
    Ipv6Address m_reg_address;
};


/**
* \brief Message header for ICMPV6 over 6 LOWPAN DAC (Duplicate address Confirmation)
*/
class Icmpv6SixLowpanDAC: public Icmpv6SixLowpanHeader
{
public:
    /**
    * \brief Constructor.
    */
    Icmpv6SixLowpanDAC();
    /**
    * \brief Destructor.
    */
    virtual ~Icmpv6SixLowpanDAC();

    /**
    * \brief Get the UID of this class.
    * \return UID
    */
    static TypeId GetTypeId ();

    /**
    * \brief Get the instance type ID.
    * \return instance type ID
    */
    virtual TypeId GetInstanceTypeId () const;

    /**
    * \brief Print informations.
    * \param os output stream
    */
    virtual void Print (std::ostream& os) const;

    /**
    * \brief Get the serialized size.
    * \return serialized size
    */
    virtual uint32_t GetSerializedSize () const;

    /**
    * \brief Serialize the packet.
    * \param start start offset
    */
    virtual void Serialize (Buffer::Iterator start) const;

    /**
    * \brief Deserialize the packet.
    * \param start start offset
    * \return length of packet
    */
    virtual uint32_t Deserialize (Buffer::Iterator start);

    /**
    * \brief Get the reserved field.
    * \return reserved value
    */
    void SetResereved(uint8_t reserved);

    /**
    * \brief Set the reserved field.
    * \param reserved the reserved value
    */
    uint8_t GetReserved() const;

    /**
    * \brief Set the status field.
    * \param status the status value
    */
    void SetStatus(uint8_t status);

    /**
    * \brief Get the status field.
    * \return status value
    */
    uint8_t GetStatus() const;

    /**
    * \brief Set the status field.
    * \param status the status value
    */
    void SetRegtrationLifetime(uint16_t reg_lifetime);

    /**
    * \brief Get Registration Lifetime
    * \return Registration lifetime value
    */
    uint16_t GetRegistrationLifetime() const;

    /**
    * \brief Get Eui64 value
    * \param eui64 the eui64 value
    */
    void SetEui64(uint64_t eui64);

    /**
    * \brief Get the Eui64 value
    * \return eui64 the eui64 value
    */
    uint64_t GetEui64() const;

    /**
    * \brief Set host address that was contained in IpV6 source field
    * \param reg_address the host address value
    */
    void SetRegisteredAddress(Ipv6Address reg_address);

    /**
    * \brief Get Registered address
    * \return Ipv6Address the host address
    */
    Ipv6Address GetRegisteredAddress() const;

private:

    /**
    *   \brief This field is unused. It MUST be initialized
    *           to zero by the sender and MUST be ignored by the receiver
    */
    uint8_t m_reserved;

    /**
    * \brief 8-bit unsigned integer. Indicates the status of a registration in the DAC.
             MUST be set to 0 in the DAR
    */
    uint8_t m_status;

    /**
    * \brief 16-bit unsigned integer. The amount of time
    *        in units of 60 seconds that the 6LBR should
    *        retain the DAD table entry for the Registered
    *        Address. A value of 0 indicates in a DAR that
    *        the DAD table entry should be removed.
    */
    uint16_t m_reg_lifetime;

    /**
    * \brief 64 bits. This field is used to uniquely
    *        identify the interface of the Registered
    *        Address by including the EUI-64 identifier
    *        [EUI64] assigned to it unmodified
    */
    uint64_t m_eui64;

    /**
    * \brief 128-bit field. Carries the host address that
    *       was contained in the IPv6 Source field in the
    *       NS that contained the ARO sent by the host.
    */
    Ipv6Address m_reg_address;
};



/**
* \brief Option header for ICMPV6 over 6 LOWPAN ARO (Address Registration option)
*/
class Icmpv6SixLowpanOptionARO: public Icmpv6SixLowpanOptionHeader
{
public:

    enum StatusType_e{

        /* \brief Success */
        ICMPV6_LOWPAN_ARO_OPTION_SUCCESS    = 0,

        /* \brief Duplicate Address*/
        ICMPV6_LOWPAN_ARO_OPTION_DUPLADD    = 1,

        /* \brief Neighbor Cache Full*/
        ICMPV6_LOWPAN_ARO_OPTION_NCACHEFULL = 2,
    };


    /**
    * \brief Get the instance type ID.
    * \return instance type ID
    */
    virtual TypeId GetInstanceTypeId () const;

    /**
    * \brief Constructor.
    */
    Icmpv6SixLowpanOptionARO ();

    /**
    * \brief Destructor.
    */
    virtual ~Icmpv6SixLowpanOptionARO ();

    /**
    * \brief Print information.
    * \param os output stream
    */
    virtual void Print (std::ostream& os) const;

    /**
    * \brief Get the serialized size.
    * \return serialized size
    */
    virtual uint32_t GetSerializedSize () const;

    /**
    * \brief Serialize the packet.
    * \param start start offset
    */
    virtual void Serialize (Buffer::Iterator start) const;

    /**
    * \brief Deserialize the packet.
    * \param start start offset
    * \return length of packet
    */
    virtual uint32_t Deserialize (Buffer::Iterator start);

    /**
    * \brief Set the status field.
    * \param status the status value
    */
    void SetStatus(uint8_t status);

    /**
    * \brief Get the status field.
    * \return status value
    */
    uint8_t GetStatus() const;

    /**
    * \brief Get the reserved field.
    * \return reserved value
    */
    void SetResereved(uint8_t reserved);

    /**
    * \brief Set the reserved field.
    * \param reserved the reserved value
    */
    uint8_t GetReserved() const;

    /**
    * \brief Set the registration lifetime field.
    * \param status the registration lifetime value
    */
    void SetRegtrationLifetime(uint16_t reg_lifetime);

    /**
    * \brief Get Registration Lifetime
    * \return Registration lifetime value
    */
    uint16_t GetRegistrationLifetime() const;

    /**
    * \brief Get Eui64 value
    * \param eui64 the eui64 value
    */
    void SetEui64(uint64_t eui64);

    /**
    * \brief Get the Eui64 value
    * \return eui64 the eui64 value
    */
    uint64_t GetEui64() const;

private:

    /**
    * \brief 8-bit unsigned integer. Indicates the status
    * of a registration in the NA response. MUST
    * be set to 0 in NS messages
    */
    uint8_t m_status;

    /**
    * \brief This field is unused. It MUST be initialized
    * to zero by the sender and MUST be ignored by
    * the receiver
    */
    uint16_t m_reserved;

    /**
    * \brief 16-bit unsigned integer. The amount of time
    * in units of 60 seconds that the router should
    * retain the NCE for the sender of the NS that
    * includes this option
    */
    uint16_t m_reg_lifetime;

    /**
    * \brief 64 bits. This field is used to uniquely
    * identify the interface of the Registered
    * Address by including the EUI-64 identifier
    * [EUI64] assigned to it unmodified
    */
    uint64_t m_eui64;

};

/**
* \brief Option header for ICMPV6 over 6 LOWPAN 6CO (6LoWPAN Context Option)
*/
class Icmpv6SixLowpanOption6CO: public Icmpv6SixLowpanOptionHeader
{
public:
    /**
    * \brief Get the UID of this class.
    * \return UID
    */
    static TypeId GetTypeId ();

    /**
    * \brief Get the instance type ID.
    * \return instance type ID
    */
    virtual TypeId GetInstanceTypeId (void) const;

    /**
    * \brief Constructor.
    */
    Icmpv6SixLowpanOption6CO ();

    /**
    * \brief Destructor.
    */
    virtual ~Icmpv6SixLowpanOption6CO ();

    /**
    * \brief Print information.
    * \param os output stream
    */
    virtual void Print (std::ostream& os) const;

    /**
    * \brief Get the serialized size.
    * \return serialized size
    */
    virtual uint32_t GetSerializedSize () const;

    /**
    * \brief Serialize the packet.
    * \param start start offset
    */
    virtual void Serialize (Buffer::Iterator start) const;

    /**
    * \brief Deserialize the packet.
    * \param start start offset
    * \return length of packet
    */
    virtual uint32_t Deserialize (Buffer::Iterator start);

    /**
    * \brief Get the reserved field.
    * \return reserved value
    */
    void SetResereved(uint16_t reserved);

    /**
    * \brief Set the reserved field.
    * \param reserved the reserved value
    */
    uint8_t GetReserved() const;

    /**
    * \brief Set the status field.
    * \param status the status value
    */
    void SetStatus(uint8_t status);

    /**
    * \brief Get the status field.
    * \return status value
    */
    uint8_t GetStatus() const;

    /**
    * \brief Set the context length
    * \param  context length value
    */
    void SetContextLen(uint8_t context_len);

    /**
    * \brief Get the context length
    * \return context length value
    */
    uint8_t GetContextLen() const;

    /**
    * \brief Set the C flag value
    * \param  C flag value
    */
    void SetCFlag(bool c_flag);

    /**
    * \brief Get the C flag
    * \return C flag value
    */
    bool GetCFlag() const;

    /**
    * \brief Set the Context ID
    * \param  Context ID value
    */
    void SetContextId(uint8_t cid);

    /**
    * \brief Get the Context ID
    * \return Context ID value
    */
    uint8_t GetContextId() const;

    /**
    * \brief Set the Res value
    * \param Res value
    */
    void SetRes(uint8_t res);

    /**
    * \brief Get the Res value
    * \return Res value
    */
    uint8_t GetRes() const;

    /**
    * \brief Set the Valid life time
    * \param Valid life time value
    */
    void SetValidLifetime(uint16_t valida_lifetime);

    /**
    * \brief Get the Valid life time value
    * \return Valid life value
    */
    uint8_t GetValidLifetime() const;

    /**
    * \brief Set the Context prefix
    * \param Context prefix value
    */
    void SetContextPrefix(std::vector<uint8_t> context_prefix);

    /**
    * \brief Get the Context prefix
    * \return Context prefix value
    */
    std::vector<uint8_t> GetContextPrefix() const;

private:

    /**
    * \brief 8-bit unsigned integer. The number of leading bits
    * in the Context Prefix field that are valid. The
    * value ranges from 0 to 128. If it is more than 64,
    * then the Length MUST be 3
    */
    uint8_t m_context_len;

    /**
    * \brief 1-bit context Compression flag. This flag indicates
    * if the context is valid for use in compression. A
    * context that is not valid MUST NOT be used for
    * compression but SHOULD be used in decompression in
    * case another compressor has not yet received the
    * updated context information. This flag is used to
    * manage the context life cycle based on the
    * recommendations
    */
    bool m_c_flag;

    /**
    * \brief 4-bit Context Identifier for this prefix
    * information. The CID is used by context-based
    * header compression as specified in [RFC6282]. The
    * list of CIDs for a LoWPAN is configured on the 6LBR
    * that originates the context information for the
    * 6LoWPAN.
    */
    uint8_t m_cid;

    /**
    * \brief This field is unused. It MUST be initialized to
    * zero by the sender and MUST be ignored by the receiver.
    */
    uint8_t m_res;


    /**
    * \brief This field is unused. It MUST be initialized to
    * zero by the sender and MUST be ignored by the receiver.
    */
    uint16_t m_reserved;

    /**
    * \brief 16-bit unsigned integer. The length of time in
    * units of 60 seconds (relative to the time the packet
    * is received) that the context is valid for the
    * purpose of header compression or decompression. A
    * value of all zero bits (0x0) indicates that this
    * context entry MUST be removed immediately.
    */
    uint16_t m_valid_lifetime;

    /**
    * \brief The IPv6 prefix or address corresponding to the CID
    * field. The valid length of this field is included
    * in the Context Length field. This field is padded
    * with zeros in order to make the option a multiple of
    * 8 bytes.
    */
    std::vector<uint8_t> m_context_prefix;

	/**
    * \brief status
	*/
	uint8_t m_status;
};


/**
* \brief Option header for ICMPV6 over 6 LOWPAN ABRO (Authoritative Border Router Option)
*/
class Icmpv6SixLowpanOptionABRO: public Icmpv6SixLowpanOptionHeader
{
public:
    /**
    * \brief Constructor.
    */
    Icmpv6SixLowpanOptionABRO();

    /**
    * \brief Get the UID of this class.
    * \return UID
    */
    static TypeId GetTypeId ();

    /**
    * \brief Get the instance type ID.
    * \return instance type ID
    */
    virtual TypeId GetInstanceTypeId (void) const;

    /**
    * \brief Destructor.
    */
    virtual ~Icmpv6SixLowpanOptionABRO();
    /**
    * \brief Print information.
    * \param os output stream
    */
    virtual void Print (std::ostream& os) const;

    /**
    * \brief Get the serialized size.
    * \return serialized size
    */
    virtual uint32_t GetSerializedSize () const;

    /**
    * \brief Serialize the packet.
    * \param start start offset
    */
    virtual void Serialize (Buffer::Iterator start) const;

    /**
    * \brief Deserialize the packet.
    * \param start start offset
    * \return length of packet
    */
    virtual uint32_t Deserialize (Buffer::Iterator start);

    /**
    * \brief Set the Version low value
    * \param version_low Version Low value
    */
    void SetVersionLow(uint16_t version_low);

    /**
    * \brief Get the Version low value
    * \return Version low value
    */
    uint16_t GetVersionLow() const;

    /**
    * \brief Set Version High value
    * \param version_high Version High value
    */
    void SetVersionHigh(uint16_t version_high);

    /**
    * \brief Get the Version High value
    * \return Version High value
    */
    uint16_t GetVersionHigh() const;

    /**
    * \brief Set the Valid life time value
    * \param valid_lifetime Set the Valid life time value
    */
    void SetValidLifetime(uint16_t valid_lifetime);

    /**
    * \brief Get the valid life time value
    * \return valid life time value
    */
    uint16_t GetValidLifetime() const;

    /**
    * \brief Set the 6BLR address
    * \param sixblr_address 6BLR address value
    */
    void Set6BlrAddress(Ipv6Address sixblr_address);

    /**
    * \brief Get the 6BLR address value
    * \return 6BLR address value
    */
    Ipv6Address Get6BlrAddress() const;

private:

    /**
    * \brief    Together, Version Low and Version High
    * constitute the Version Number field, a
    * 32-bit unsigned integer where Version Low
    * is the least significant 16 bits and
    * Version High is the most significant
    * 16 bits. The version number
    * corresponding to this set of information
    * contained in the RA message. The
    * authoritative 6LBR originating the prefix
    * increases this version number each time
    * its set of prefix or context information changes.
    */
    uint16_t m_version_low;

    /**
    * \brief    Together, Version Low and Version High
    * constitute the Version Number field, a
    * 32-bit unsigned integer where Version Low
    * is the least significant 16 bits and
    * Version High is the most significant
    * 16 bits. The version number
    * corresponding to this set of information
    * contained in the RA message. The
    * authoritative 6LBR originating the prefix
    * increases this version number each time
    * its set of prefix or context information changes.
    */
    uint16_t m_version_high;

    /**
    * \brief    16-bit unsigned integer. The length of
    * time in units of 60 seconds (relative to
    * the time the packet is received) that
    * this set of border router information is
    * valid. A value of all zero bits (0x0)
    * assumes a default value of 10,000 (~one week).
    */
    uint16_t m_valid_lifetime;

    //reserved filed to check

    /**
    * \brief    IPv6 address of the 6LBR that is the
    * origin of the included version number
    */
    Ipv6Address m_6blr_address;

};

}; /*namespace ns3*/

#endif //ICMPV6_SIXLOWPAN_HEADER_H