#include "ns3/log.h"
#include "ns3/assert.h"
#include "ns3/packet.h"
#include "ns3/node.h"
#include "ns3/boolean.h"
#include "ns3/ipv6-routing-protocol.h"
#include "ns3/ipv6-route.h"
#include "ns3/pointer.h"
#include "ns3/string.h"

#include "ns3/ipv6-interface.h"
#include "ns3/ndisc-cache.h"

#include "icmpv6-sixlowpan-header.h"
#include "icmpv6-sixlowpan-protocol.h"



namespace ns3{

NS_OBJECT_ENSURE_REGISTERED (Icmpv6SixLowPanProtocol);

NS_LOG_COMPONENT_DEFINE ("Icmpv6SixLowPanProtocol");

const uint8_t Icmpv6SixLowPanProtocol:: MIN_CONTEXT_CHANGE_DELAY          = 300; //300 seconds
const uint8_t Icmpv6SixLowPanProtocol:: MAX_RTR_ADVERTISEMENTS            = 3;
const uint8_t Icmpv6SixLowPanProtocol:: MIN_DELAY_BETWEEN_RAS             = 10;  //10 seconds
const uint8_t Icmpv6SixLowPanProtocol:: MAX_RA_DELAY_TIME                 = 2;    //2 seconds
const uint8_t Icmpv6SixLowPanProtocol:: TENTATIVE_NCE_LIFETIME            = 20;   //20 seconds
const uint8_t Icmpv6SixLowPanProtocol:: MULTIHOP_HOPLIMIT                 = 64;
const uint8_t Icmpv6SixLowPanProtocol:: MAX_RTR_SOLICITATIONS             = 10;   //10 secomnds
const uint8_t Icmpv6SixLowPanProtocol:: RTR_SOLICITATION_INTERVAL         = 3;
const uint8_t Icmpv6SixLowPanProtocol:: MAX_RTR_SOLICITATION_INTERVAL     = 60;   //60 seconds


TypeId Icmpv6SixLowPanProtocol:: GetTypeId()
{
    static TypeId tid = TypeId ("ns3::Icmpv6SixLowPanProtocol")
        .SetParent<IpL4Protocol> ()
        .AddConstructor<IpL4Protocol> ()
        .AddAttribute ("DAD", "Always do DAD check.",
                BooleanValue (true),
                MakeBooleanAccessor (&Icmpv6SixLowPanProtocol::m_alwaysDad),
                MakeBooleanChecker ())
        .AddAttribute ("SolicitationJitter", "The jitter in ms a node is allowed to wait before sending any solicitation . Some jitter aims to prevent collisions. By default, the model will wait for a duration in ms defined by a uniform random-variable between 0 and SolicitationJitter",
                       StringValue ("ns3::UniformRandomVariable[Min=0.0|Max=10.0]"),
                       MakePointerAccessor (&Icmpv6SixLowPanProtocol::m_solicitationJitter),
                       MakePointerChecker<RandomVariableStream> ());

    return tid;
}

Icmpv6SixLowPanProtocol::Icmpv6SixLowPanProtocol ()
  : m_node (0)
{
  NS_LOG_FUNCTION (this);
}

Icmpv6SixLowPanProtocol::~Icmpv6SixLowPanProtocol ()
{
  NS_LOG_FUNCTION (this);
}


Ptr<Packet> Icmpv6SixLowPanProtocol:: ForgeRS (Ipv6Address src, Ipv6Address dst, Address hardwareAddress)
{
    NS_LOG_FUNCTION(this << src << dst << hardwareAddress);
    Ptr<Packet> p = Create<Packet> ();

    Ipv6Header ipHeader;
    Icmpv6RS rs;
    Icmpv6OptionLinkLayerAddress llOption (1, hardwareAddress);  /* we give our mac address in response */

    NS_LOG_LOGIC ("Send RS ( from " << src << " to " << dst << ")");
    p->AddHeader (llOption);

    rs.CalculatePseudoHeaderChecksum (src, dst, p->GetSize () + rs.GetSerializedSize (), PROT_NUMBER);
    p->AddHeader (rs);

    ipHeader.SetSourceAddress (src);
    ipHeader.SetDestinationAddress (dst);
    ipHeader.SetNextHeader (PROT_NUMBER);
    ipHeader.SetPayloadLength (p->GetSize ());
    ipHeader.SetHopLimit (255);

    p->AddHeader (ipHeader);

    return p;
}

Ptr<Packet> Icmpv6SixLowPanProtocol:: ForgeNS (Ipv6Address src, Ipv6Address dst, Ipv6Address target, Address hardwareAddress)
{
  NS_LOG_FUNCTION (this << src << dst << target << hardwareAddress);
  Ptr<Packet> p = Create<Packet> ();
  Ipv6Header ipHeader;
  Icmpv6SixLowpanOptionARO aro();
  Icmpv6NS ns (target);
  Icmpv6OptionLinkLayerAddress llOption (1, hardwareAddress);  /* we give our mac address in response */

  /* if the source is unspec, An unspecified source address MUST NOT be used in NS messages (section 5.5.1 rfc 6775)*/
  if (src == Ipv6Address::GetAny ())
    {
		NS_LOG_FUNCTION(this << "Unspecified soruce address");
		return;
    }

  NS_LOG_LOGIC ("Send NS ( from " << src << " to " << dst << " target " << target << ")");

  p->AddHeader (llOption);  
  ns.CalculatePseudoHeaderChecksum (src, dst, p->GetSize () + ns.GetSerializedSize (), PROT_NUMBER);
  p->AddHeader (ns);
  p->AddHeader(aro);

  ipHeader.SetSourceAddress (src);
  ipHeader.SetDestinationAddress (dst);
  ipHeader.SetNextHeader (PROT_NUMBER);
  ipHeader.SetPayloadLength (p->GetSize ());
  ipHeader.SetHopLimit (255);

  p->AddHeader (ipHeader);

  return p;
}

}// NS3